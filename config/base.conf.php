<?
######################################
# Автор:    Binali Rustamov          #
# Лицензия: GPL (любой редакции)     #
# Копирайт: (c) 2014 Abitech         #
# Email:    rrbb_wapmaster@mail.ru   #
#     Job System v 0.1 alfa          #
######################################
/*
 Здесь расположены важные конфигурации
 */
define('HOST','localhost'); //Хост
define('USER','root'); //Пользователь
define('PASSWORD',''); //Пароль
define('DB','time_of_work'); //БД
define('ON_PAGE','10'); //Число записей на страницу
?>
