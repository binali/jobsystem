<?
######################################
# Автор:    Binali Rustamov          #
# Лицензия: GPL (любой редакции)     #
# Копирайт: (c) 2014 Abitech         #
# Email:    rrbb_wapmaster@mail.ru   #
#     Job System v 0.1 alfa          #
######################################
/*
 Скрипт запуска приложения
*/
if(is_file('./config/base.conf.php')) //проверка наличия файла конфигурации
{
date_default_timezone_set("Asia/Almaty"); //установка временной зоны
require './classes/MySQL.class.php'; //подключение класса MySQL
require './classes/User.class.php'; //подключение класса User
require './classes/Actions.class.php'; //подключение класса Actions
	require './config/base.conf.php'; //поключение конфигурационного файла
	$db=new MySQL(HOST,USER,PASSWORD,DB); //создание экземпляра класса MySQL
	if(!$db->getErrors()) //проверка подключения
	{
		  $user=new User($db); //создание экземпляра класса User
		  if(!$user->isUser()&&!@$auth) //если вход не произведен и пользователь находится не на странице авторизации
		  {
			  header('location: auth.php'); //переадресовать на страницу авторизации
		  }
	}
	else
	{
		die('Не удалось подключиться к базе данных. Пожалуйста попробуйте снова!');
	} 
}
else
{
	die('Установите, файл конфигурации!');
}
?>
