<?
######################################
# Автор:    Binali Rustamov          #
# Лицензия: GPL (любой редакции)     #
# Копирайт: (c) 2014 Abitech         #
# Email:    rrbb_wapmaster@mail.ru   #
#     Job System v 0.1 alfa          #
######################################
/*
 Действия над заданиями
*/
		#########################-НАЧАЛО ДЕЙСТВИЙ НАД ЗАДАНИЯМИ-#################
		switch(@$_GET['make']) //назначить переменную переключателю switch
		{
			case 'delete': //действие удалить
			if($user->isAdmin()&&is_numeric($_GET['del_id'])) //если пользователь админ и параметр число
			{
				if(isset($_GET['del_ok'])&&$_GET['del_ok']==='yes') //если действие подтверждено
				{
					if($action->deleteJob($_GET['del_id'])) //если задание удалено
					{
						$okey[]='Задание успешно удалено!'; // предупреждение
					}
					else
					{
						$error[]='Произошла ошибка, попробуйте снова'; //ошибка
					}
				}
				else
				{ //если действие не подтверждено
					$del_id=(int)$_GET['del_id']; //присвоить значение
					$additional_form='question_delete'; //создать форму с вопросом
				}
			}
			break; //конец действия удаление задания
			
			case 'edit'://действие редактирование задания
			if($user->isAdmin()&&is_numeric($_GET['edit_id'])&&$action->isJob($_GET['edit_id']))//если пользователь админ и задание есть в базе
			{
				$additional_form='edit_job'; //назначить форму
				$users=$action->getUserNamesAsArray(); //получить имена людей как массив [id]=name
				$row=$action->getJobById($_GET['edit_id']);//получить данные задания
				//Присвоить
				$record_id=$row['id'];
				$executor_id=$row['executor_id'];
				$author_comment=$row['author_comment'];
				//
						//----------------ОБРАБОТКА ПОЛЕЙ ФОРМЫ---------------//
				//поле Исполнитель
				if(isset($_POST['executor_id'])&&$action->isUser($_POST['executor_id'])) //если пользователь с таким id существует
					{
						if((int)$_POST['executor_id']!=$executor_id) //если поле изменено
						{
						$action->updateJob($record_id,$_POST['executor_id']); //обновить
						$okey[]='Исполнитель задания успешно измнен'; //предупреждение
						}
						$good=true; //флаг все в порядке
					}				
				//комментарий автора
				if(isset($_POST['author_comment'])&&$action->checkComment($_POST['author_comment'])) //если значение корректно
					{
						if($_POST['author_comment']!==$author_comment) //если значение изменено
						{
						$author_comment=htmlspecialchars($_POST['author_comment']); //закодировать все спец.символы html
						$action->updateJob($record_id,$author_comment,'author_comment'); // обновить
						$okey[]='Комментарий успешно измнен';//предупреждение
						}
						$good=true; //флаг все в порядке
					}
					if(isset($good)) //если изменения прошли успешно
					{
						unset($additional_form); //уничтожить форму
					}
			}
			break; //конец действия изменить задание
			
			case 'new'://действие создать задание
			if($user->isAdmin()) //если админ
			{
				$additional_form='new_job'; //назначить форму
				$users=$action->getUserNamesAsArray(); //получить имена сотрудников как массив [id]=name
				if(isset($_POST['posted'])) //если кнопка Создать кликута
				{
						//------------------------ОБРАБОТКА ЗНАЧЕНИЙ ФОРМЫ----------//
					//ID исполнителя	
					if(isset($_POST['executor_id'])&&$action->isUser($_POST['executor_id'])) //если такой пользователь есть в базе
					{
						$executor_id=(int)$_POST['executor_id']; // присвоить значение
					}
					else
					{ //если ошибка
						$err[]='Необходимо указать исполнителя задания'; //ошибка
					}
					//Комментарий автора
					if(isset($_POST['author_comment'])&&$action->checkComment($_POST['author_comment'])) //если заполнен правильно
					{
						$author_comment=htmlspecialchars($_POST['author_comment']); //закодировать спец. символы html и присвоить значение
					}
					else
					{//если ошибка
						$err[]='Не указан комментарий, или вы используете запрещенные символы'; //ошибка
					}
					if(!isset($error)) //если ошибок в полях нет
					{
						$action->createJob($executor_id,$author_comment); //создать запись
						$okey[]='Задание успешно создано'; //предупрждение
						unset($additional_form); //уничтожить форму
					}
				}
			}
			break; //конец действия создание задания
			//---------------ИЗМЕНЕНИЕ СТАТУСА ЗАДАНИЯ-------//
			case 'checked': //установить флаг просмотрено
			if($user->isAdmin()&&is_numeric($_GET['edit_id'])) //если админ и данные верны
			{
				$action->checkJob($_GET['edit_id']); //установить флаг
				$okey[]='Статус задания успешно изменен'; //предупрждение
			}
			break; //конец действия
			
			case 'start': //установить флаг исполняемого
			if(!$user->isAdmin()&&is_numeric($_GET['edit_id'])) //если не админ и данные верны
			{
				$action->startJob($_GET['edit_id']); //установить флаг
				$okey[]='Статус задания успешно изменен'; //предупреждение
			}
			break;//конец действия
			
			case 'finish': //действие установить флаг выполненного задания
			if(!$user->isAdmin()&&is_numeric($_GET['edit_id'])&&$action->isJob($_GET['edit_id'])) //если не админ и данные верны
			{
				$record_id=(int)$_GET['edit_id']; //получить id задания
				$additional_form='finish_job'; //назначить форму
				//ОБРАБОТКА ФОРМЫ
				if(isset($_POST['comment'])&&Actions::checkComment($_POST['comment'])) //если данные корректны
 				{
				unset($additional_form); //уничтожить форму
				$executor_comment=htmlspecialchars($_POST['comment']); //закодировать спец. символы
				$action->finishJob($_GET['edit_id'],$executor_comment);//установить флаг
				$okey[]='Статус задания успешно изменен';//предупреждение
				}
				//
			}
			break; //конец действия
		}
		#####################################-КОНЕЦ ДЕЙСТВИЙ НАД ЗАДАНИЯМИ-###########################
		?>
