<?
######################################
# Автор:    Binali Rustamov          #
# Лицензия: GPL (любой редакции)     #
# Копирайт: (c) 2014 Abitech         #
# Email:    rrbb_wapmaster@mail.ru   #
#     Job System  v0.1 alfa          #
######################################
/*
 Страница авторизации
*/
$auth=true; //установить флаг страницы авторизации
require './config/init.php'; //подключить скрипт запуска
if($user->isUser()) //если пользователь авторизован
{
header('location: index.php'); //отправить на главную страницу
}
else
{
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">

<head>
	<title>Авторизация - Job System</title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<style type="text/css">
	 body {width:800px;
		   margin: 0 auto; 
		   padding-top: 10%; 
		   background-color: lightgray}
.biglabel {background-color:gray;
		   color:white;
		   border:4px solid white; 
		   margin-left:20%; 
		   margin-right:20%}
.label	  {border-bottom:4px solid white;
		   border-left:4px solid white;
		   border-right:4px solid white;
		   margin-left:20%;
		   margin-right:20%}
.form     {text-align:center;
		   background-color: #E5E5E5;
		   border-bottom:4px solid white;
		   border-left:4px solid white;
		   border-right:4px solid white; 
		   border-radius: 0 0  10px 10px; 
		   margin-left:20%; 
		   margin-right:20%;
		   padding-top:2%}
 
	</style>
</head>
<body>
<div align="center" class="biglabel"> <H2>Job System  <sup>alfa</sup></H2>
</div>
<div align="center" class="label">
<b><i>Авторизация</i></b>
</div>
<form action="index.php" method="post" class="form">
<label>E-mail:</label> <br />
<input type="email" name="email" value="" required /> <br />
<label>Пароль:</label> <br />
<input type="password" name="password" value="" required /> <hr />
<input type="submit" value="Авторизоваться" />
<hr />
</form>
<br />
<br />
<div align="center">
&copy; 2014  Binali Rustamov special for Abitech	
</div>
</body>

</html>
<?
}
?>
