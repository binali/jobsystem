<div class="minimenu"><a href="?act=executors&make=new<?=$current_page ?>" title="Добавление сотрудника в базу данных">Добавить нового сотрудника</a></div>
<? if(isset($additional_form)) //если нужно создать форму
	{
	include "./views/executors/".$additional_form.".php";
	}
  if(is_array($executors)) //если есть данные
   {
    ?>
    <table width="100%" class="table">
	<tr>
	<td><b>ID</b></td>
	<td><b>ФИО</b></td>
	<td><b>ИИН</b></td>
	<td><b>E-mail</b></td>
	<td><b>Тел.</b></td>
	<td><b>Cтатус</b></td>
	<td><b>Последняя активность</b></td>
    <td><b>Действия</b></td>
	</tr>
    <?
    foreach($executors as $executor)
    {
	?><tr><?
	echo "<td>".$executor['id']."</td>";
	if($user->isAdmin())
	{
    echo "<td>".$executor['fio']."</td>";
	}
	echo "<td>".$executor['iin']."</td>";
	echo "<td><a href=\"mailto:".$executor['email']."\" title=\"Написать письмо\">".$executor['email']."</a></td>";
	echo "<td>+7".$executor['tel']."</td>";
	echo "<td>".($executor['status']==1?'Администратор':'Исполнитель')."</td>";
	echo "<td>".$executor['last_time']."</td>";
	echo "<td>";
	 ?>
		<a href="?act=executors&make=edit&edit_id=<?=$executor['id'].$current_page ?>" title="Изменить данные сотрудника"> [Изм.]</a>
	 <?
	if($user->getUserId()!=$executor['id'])
	{
		?> <a href="?act=executors&make=delete&del_id=<?=$executor['id'].$current_page ?>" title="Удалить этото сотрудника"> [Уд.]</a> <?
	}
	echo "</td>";
	?></tr><?	
	}
	?>
	</table>
	<?
if(isset($pagenavi)) //если есть навигатор
	{
		echo $pagenavi->getNavigator();	 //получить
	}
}
  else
{
	echo "В данный момент сотрудников нет!";
}
?>
