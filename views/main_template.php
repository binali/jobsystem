<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">

<head>
	<title><?=$page_title?> - Job System </title>
	<meta http-equiv="content-type" content="text/html;charset=utf-8" />
	<style type="text/css">
	 body {width:1024px;
		   margin: 0 auto; 
		   padding-top: 1%; 
		   background-color: lightgray}
.biglabel {padding-left:5px;
		   background-color:gray;
		   color:white;
		   border:4px solid white;
		   padding-bottom:1%;
		   }
.label	  {
	       text-align: center;
		   border-bottom:4px solid white;
		   border-left:4px solid white;
		   border-right:4px solid white;}
.error	  {text-align: center;
		   margin-top:2px;
		   margin-bottom:2px;
		   border:2px solid grey;
		   color: red;
		   font-size:12px;}
.okey	  {text-align: center;
		   margin-top:2px;
		   margin-bottom:2px;
		   border:2px solid green;
		   font-size:12px;}
.content  {text-align:justify;
		   background-color: #E5E5E5;
		   border-bottom:6px solid white;
		   border-left:4px solid white;
		   border-right:4px solid white; 
		   border-radius: 0 0  14px 14px; 
		   padding-top:2%;
		   padding-left:2%;
		   padding-bottom:5%}
.content a:link { color:black;
				}
.content a:hover { color:gray;
			    }
a:link    { color:white
          }
a:hover   { color:gold
          }
a:visited   {
	        color:gold
		  }
.menu 	  {
		   padding:2px;
	       border:2px solid white;
	       border-radius: 6px 14px;
	       
	      }
.menu:hover
		{ 
		  border:2px solid gold;
		  color:gold;
		 }
.menu a:link { color:white;
			   padding: 10px;
				}
.menu a:hover { color:gold;
	            padding: 10px;
			    }
.minimenu { text-align:center;
			width:80%;
			padding-bottom:5px;
			margin-bottom:20px;
			border: 2px solid white;
			margin: 0 auto;
			margin-top:-20px;
			margin-bottom:5px;
			border-radius:0 0  30px 30px;
			background-color: gray;
				}
.minimenu a:hover {
			color: white;
	}
.table {font-size: 12px; border:1px solid gray}
.table td {border:1px solid gray;}
div.navigator{
			font-size:12px;
			padding-top:15px;
			text-align:center;
		}
div.totalpagesdisplay{
			padding-top:5px;
			font-size:14px;
			text-align:center;
			font-style:italic;
		}
.navigator a, span.inactive{
			padding : 0px 5px 2px 5px;
			margin-left:0px;
			border-top:1px solid #999999;
			border-left:1px solid #999999;
			border-right:1px solid #000000;
			border-bottom:1px solid #000000;
			border-radius:5px;
		}
.navigator a:link, .navigator a:visited,
.navigator a:hover,.navigator a:active{
			color: #3300CC;
			background-color: #FAEBF7;
			text-decoration: none;
		}
		span.inactive{
			background-color :#EEEEEE;
		}
	</style>
</head>
<body>
<div class="biglabel"><span> <H2>Job System  <sup>alfa</sup></H2></span><span style="margin-left: 60%">Здравствуйте, <?=$user->getFullName() ?>!</span>
<div> 
<? if($user->isAdmin()) 
{ 
?>
<span class="menu"><a href="?act=executors" title="Просмотр и редактирование сотрудников">Сотрудники</a></span> <span class="menu"><a href="?act=jobs" title="Просмотр и редактирование заданий">Задания</a></span> 
<span style="margin-left: 50%">
<?
}
else
{
?>
<span class="menu"><a href="?act=jobs" title="Просмотр и редактирование заданий">Задания</a></span> 
<span style="margin-left: 70%">
<?	
}
?>
 <a href="?act=showinfo" title="Просмотреть свои данные">Мои данные</a> | <a href="?act=exit" title="Закрыть текущую сессию">Выход</a></span></div>
</div>
<div class="label">
<b><i><?=$page_title ?></i></b>
</div>
<?
if(isset($error))
{
?>
<div class="error">
<?
foreach($error as $value)
{
echo $value."<br />"; 	
}
?>
</div>
<?
}
?>
<?
if(isset($okey))
{
?>
<div class="okey">
<?
foreach($okey as $value)
{
echo $value."<br />"; 	
}
?>
</div>
<?
}
?>
<div class="content">

<?@include './views/'.$current_template.'.php';?>
</div>
<div align="center" style="padding-top:20px">
&copy; 2014  Binali Rustamov special for Abitech	
</div>
</body>

</html>
