<div class="minimenu">
  <?=$user->isAdmin()?'<a href="?make=new" title="Создать задание для сотрудника">Создать задание</a> |':null //пункт только админам ?>
    <a href="?show=all" title="Просмотреть все задания">Просмотреть всё</a> 
  | <a href="?show=lastcompleted" title="Просмотреть последние выполненные задания">Последние выполненные</a> 
  | <a href="?show=new" title="Посмотреть новые задания">Новые задания</a></div>
<? if(isset($additional_form)) //если есть форма
	{
	include "./views/jobs/".$additional_form.".php";
	}
  if(is_array($jobs)) //если есть записи
   {
    ?>
    <table width="100%" class="table">
	<tr>
	<td><b>ID</b></td>
	<? if($user->isAdmin())
	{?>
	<td><b>Исполнитель</b></td>
	<?
	}
	?>
	<td><b>Старт выполнения</b></td>
	<td><b>Завершение выполнения</b></td>
	<td><b>Затраты времени</b></td>
	<td><b>Комментарий автора</b></td>
	<td><b>Комментарий исполнителя</b></td>
    <td><b>Статус</b></td>
    <td><b>Действия</b></td>
	</tr>
    <?
    foreach($jobs as $job)
    {
	?><tr><?
	echo "<td>".$job['id']."</td>";
	if($user->isAdmin())
	{
    echo "<td>".$job['fio']."</td>";
	}
	echo "<td>".$job['start_time']."</td>";
	echo "<td>".$job['finish_time']."</td>";
	echo "<td>".$job['jobtime']."</td>";
	echo "<td>".$job['author_comment']."</td>";
	echo "<td>".$job['executor_comment']."</td>";
	echo "<td>".$job['status']."</td>";
	echo "<td>";
	if($user->isAdmin())
	{
	if(isset($job['editable']))
	{
	  ?>
	  <a href="?make=edit&edit_id=<?=$job['id'].$current_page ?>" title="Изменить эту запись"> [Изм.]</a>
	  <?
    }
    elseif(!$job['checked'])
    {
	?>
	  <a href="?make=checked&edit_id=<?=$job['id'].$current_page ?>" title="Подтвердить исполнение задания"> [Подтвер.]</a>
	 <?
	}
    ?>
	  <a href="?make=delete&del_id=<?=$job['id'].$current_page ?>" title="Удалить эту запись"> [Уд.]</a>
	 <?
	}
	else
	{
    if(isset($job['started']))
	{
     ?>
	  <a href="?make=finish&edit_id=<?=$job['id'].$current_page ?>" title="Завершить задание, оставить комментарий"> [Завершить]</a>
	 <?
    }
    if(isset($job['isNew']))
	{
	  ?>
	  <a href="?make=start&edit_id=<?=$job['id'].$current_page ?>" title="Начать выполение задания"> [Начать выполнение]</a>
	 <?
    }
	}
	echo "</td>";
	?></tr><?	
	}
	?>
	</table>
	<?
	if(isset($pagenavi)) //если есть навигатор
	{ 
		echo $pagenavi->getNavigator();	 //распечатать навигатор
	}
}
  else
					  {
	echo "В данный момент заданий в этом разделе нет!";
}
?>
