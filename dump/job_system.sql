CREATE TABLE IF NOT EXISTS `jobtime` (
  `id` int(30) NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор задания',
  `executor_id` int(30) NOT NULL COMMENT 'Идентификатор сотрудника',
  `start_time` int(70) DEFAULT NULL COMMENT 'Дата, начала исполнения задания',
  `finish_time` int(70) DEFAULT NULL COMMENT 'Дата, выполнения задания',
  `author_comment` text CHARACTER SET utf8 NOT NULL COMMENT 'Комментарий автора задания',
  `executor_comment` text CHARACTER SET utf8 COMMENT 'Комментарий исполнителя',
  `checked` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Флаг проверки задания администратором',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

INSERT INTO `jobtime` (`id`, `executor_id`, `start_time`, `finish_time`, `author_comment`, `executor_comment`, `checked`) VALUES
(4, 2, 1396838585, 1396839585, 'Выполни это сейчас!', 'Все сделал!', 1),
(5, 2, 1396839604, NULL, 'Это тоже надо сделать!', NULL, 0),
(6, 2, 1396840377, 1396840406, 'И это тоже!', 'Все сделано, данные отправил по email!', 0);

CREATE TABLE IF NOT EXISTS `people` (
  `id` int(30) NOT NULL AUTO_INCREMENT COMMENT 'Идентификатор сотрудника',
  `fio` varchar(90) NOT NULL COMMENT 'ФИО сотрудника',
  `iin` varchar(12) NOT NULL COMMENT 'ИИН сотрудника',
  `email` varchar(90) NOT NULL COMMENT 'E-mail сотрудника',
  `tel` varchar(15) NOT NULL COMMENT '№ телефона сотрудника',
  `admin` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Должность сотрудника',
  `last_time` int(70) DEFAULT NULL,
  `last_ip` varchar(15) DEFAULT NULL COMMENT 'Последний IP сотрудника',
  `last_ua` varchar(100) DEFAULT NULL COMMENT 'Последний User Agent сотрудника',
  `password` text NOT NULL COMMENT 'Пароль сотрудника',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

INSERT INTO `people` (`id`, `fio`, `iin`, `email`, `tel`, `admin`, `last_time`, `last_ip`, `last_ua`, `password`) VALUES
(11, 'Иванов Дмитрий', '333344455555', 'dmitry@mail.ru', '9008004411', 0, NULL, NULL, NULL, '27183aacdcb689968f322032550ad33d'),
(2, 'Executor', '333333333333', 'executor@example.com', '7073919185', 0, 1396863222, '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:28.0) Gecko/20100101 Firefox/28.0', 'b1925939f66c2e4625aadb18cabf1cea'),
(13, 'Иванова Ирина', '332255444666', 'irishka@mail.ru', '7895544114', 1, NULL, NULL, NULL, 'e4b3a8a96ec1497ceb2173050b65fbf2'),
(14, 'Рустамов Бинали', '234543245454', 'rrbb_wapmaster@mail.ru', '7056666012', 0, NULL, NULL, NULL, 'a6d04d4c7b405e50275872528a090d8d'),
(1, 'Admin', '123456789112', 'admin@example.com', '7056666012', 1, 1396864328, '127.0.0.1', 'Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:28.0) Gecko/20100101 Firefox/28.0', '21232f297a57a5a743894a0e4a801fc3');
