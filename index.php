<?
######################################
# Автор:    Binali Rustamov          #
# Лицензия: GPL (любой редакции)     #
# Копирайт: (c) 2014 Abitech         #
# Email:    rrbb_wapmaster@mail.ru   #
#     Job System v 0.1 alfa          #
######################################
/*
 Файл приложения
*/
include './config/init.php'; //подключить скрипт запуска приложения
include './classes/PageNavigator.php';// подключить класс PageNavigator
$current_template='jobs';//назначить шаблон по умолчанию
$page_title='Задания';//назначить заголовка по умолчанию
switch(@$_GET['act']) //назначить переменную переключателю switch
{
	case 'exit': //действие выход
		$user->logOut(); //вызвать действие объекта $user
	break; //конец действия
	case 'showinfo': //действие Мои данные
		$page_title='Мои данные'; //назначить заголовок страницы
		$current_template='showinfo'; //назначить шаблон по умолчанию
	break;//конец действия
	case 'executors'://действие Сотрудники
		if($user->isAdmin()) //если Администратор
		{
			$action=new Actions($db,$user); //создать экземпляр класса Actions
			include './config/executors.actions.php'; //подключить файл действий над сотрудниками
			//Главная страница Сотрудники
			if($count=$action->getExecutorsCount()) //если записи есть
			{
				if($count>ON_PAGE) //записи не умещаются в одну страницу
				{
					
					$pagenavi=new PageNavigator($count,ON_PAGE,'&act=executors'); //создать  экземпляр класса PageNavigator
					$executors=$action->getExecutorsArray($pagenavi->getNumToStart(),ON_PAGE); //получить данные сотрудников
					$current_page="&".$pagenavi->getFirstParamName()."=".$pagenavi->getCurrentPage(); //установить параметры для ссылок
				}
				else
				{ //если помещаются на одну страницу
					$executors=$action->getExecutorsArray(0,ON_PAGE); //получить данные сотрудников
					$current_page=''; //установить параметры для ссылок
				}
			}
			$current_template='executors'; //назначить вид
		}
		else
		{ //если пользователь не админ
			$error[]='Вы не имеете доступа к разделу "Сотрудники"'; //ошибка
		}
	break; //конец страницы Сотрудники
	
	//страница Задания
	default:
		$action=new Actions($db,$user); //создать экземпляр класса Actions
	    include './config/jobs.actions.php'; //подключить файл действий над заданиями
		switch(@$_GET['show'])
		{
			case 'all':
			$type='all';
			$param='&show=all';
			break;
			case 'lastcompleted':
			$type='lastcompleted';
			$param='&show=lastcompleted';
			break;
			default:
			$type='new';
			$param='&show=new';
			break;
		}
		if($count=$action->getJobsCount($type))
		{
		if($count>ON_PAGE)
			{
			$pagenavi=new PageNavigator($count,ON_PAGE,$param);
			$jobs=$action->getJobsArray($type,$pagenavi->getNumToStart(),ON_PAGE);
			$current_page=$param."&".$pagenavi->getFirstParamName()."=".$pagenavi->getCurrentPage();
			}
			else
			{
			$jobs=$action->getJobsArray($type);
			$current_page=$param;
			}
		}	
	break;
}
include "./views/main_template.php"; //подключить основной шаблон
?>
