<?
/**
 * Класс работы с данными пользователя, выполнения авторизации, разделения ролей.
 *
 * @author Binali Rustamov
 * @copyright	Copyright (c) 2014 Abitech
 * @license GPL License
 * @todo Усиление безопасности, добавление срока годности сессии, ограничение на вход по IP адресам
 * @version 0.1 Alfa
 */
class User 
{
/**
 * User ID
 * 
 * @var integer
 * @access private
 * @internal Свойство для хранения идентификатора пользователя
 */
	private $userId;
/**
 * User FullName
 * 
 * @var string
 * @access private
 * @internal Свойство для хранения ФИО пользователя
 */
	private $userFullName;
/**
 * User IIN
 * 
 * @var string
 * @access private
 * @internal Свойство для хранения ИИН пользователя
 */
	private $userIin;
/**
 * User E-mail
 * 
 * @var string
 * @access private
 * @internal Свойство для хранения E-mail пользователя
 */
	private $userEmail;
/**
 * User Tel. No
 * 
 * @var string
 * @access private
 * @internal Свойство для хранения номера телефона пользователя
 */
	private $userTel;
/**
 * User Admin
 * 
 * @var boolean
 * @access private
 * @internal Свойство для хранения типа пользователя
 */
	private $userAdmin;
/**
 * User Last Visit Time
 * 
 * @var long
 * @access private
 * @internal Свойство для хранения timestamp последнего входа пользователя
 */
	private $userLastTime;
/**
 * User Last Visit IP
 * 
 * @var string
 * @access private
 * @internal Свойство для хранения IP последнего входа пользователя
 */
	private $userLastIP;
/**
 * User Last Visit UA
 * 
 * @var string
 * @access private
 * @internal Свойство для хранения User-Agent последнего входа пользователя
 */
	private $userLastUA;
/**
 * User Password
 * 
 * @var string
 * @access private
 * @internal Свойство для хранения пароля пользователя
 */
	private $password;
/**
 * MySQL Object
 * 
 * @var object
 * @access private
 * @internal Свойство для хранения объекта класса MySQL
 */
	private $connection;
/**
 * User Auth Flag
 * 
 * @var boolean
 * @access private
 * @internal Свойство для хранения флага авторизации пользователя
 */
	private $auth=false;
/**
 * Constructor
 * 
 * @param object MySQL $connection
 * @uses checkUser($value,$password,$type)
 * @return void
 * @internal Конструктор класса
 */
	function __construct($connection)
	{
		if(empty($connection->getErrors()))
		{
			session_name('ABITECH');
			session_start();
			$this->connection=$connection;
			if(isset($_POST['email'])&&isset($_POST['password'])&&$this->checkUser(trim($_POST['email']),md5($_POST['password'])))
			{
				if($connection->query("SELECT * FROM `people` WHERE `email`='".mysql_escape_string(trim($_POST['email']))."' AND `password`='".md5($_POST['password'])."'"))
			 	{
					$data=$connection->getRow();
					$this->userId=$data['id'];
					$this->userFullName=$data['fio'];
					$this->userIin=$data['iin'];
					$this->userEmail=$data['email'];
					$this->userTel=$data['tel'];
					$this->userAdmin=$data['admin'];
					$this->userLastTime=$data['last_time'];
					$this->userLastIP=$data['last_ip'];
					$this->userLastUA=$data['last_ua'];
					$this->password=$data['password'];
					$this->auth=true;
					$_SESSION['id']=$this->userId;
					$_SESSION['password']=$this->password;
				}	
			}
			elseif(isset($_SESSION['id'])&&isset($_SESSION['password'])&&$this->checkUser($_SESSION['id'],$_SESSION['password'],'id'))
			{
			 	if($connection->query("SELECT * FROM `people` WHERE `id`='".(int)$_SESSION['id']."' AND `password`='".mysql_escape_string($_SESSION['password'])."'"))
			 	{
					$data=$connection->getRow();
					$this->userId=$data['id'];
					$this->userFullName=$data['fio'];
					$this->userIin=$data['iin'];
					$this->userEmail=$data['email'];
					$this->userTel=$data['tel'];
					$this->userAdmin=$data['admin'];
					$this->userLastTime=$data['last_time'];
					$this->userLastIP=$data['last_ip'];
					$this->userLastUA=$data['last_ua'];
					$this->password=$data['password'];
					$this->auth=true;
				}
			 	
			}
		}
	}
/**
 * Проверка существования пользователя
 * 
 * @access private
 * @param string $value
 * @param string $password
 * @param string $type
 * @return boolean
 * @internal $type требуется для определения типа запроса: 1) Запрос при входе (email) 2) Запрос при повторном входе через $_SESSION (session)
 */	
	private function checkUser($value,$password,$type='email')
	{
			if($type=='id')
			{
				$this->connection->query("SELECT COUNT(*) FROM `people` WHERE `id`='".mysql_escape_string($value)."' AND `password`='".mysql_escape_string($password)."'");
			}
			else
			{
				$this->connection->query("SELECT COUNT(*) FROM `people` WHERE `email`='".mysql_escape_string($value)."' AND `password`='".$password."'");
			}
			if($this->connection->getResult()>0)
			{
				return true;
			}
			else
			{
				return false;
			}
	}
/**
 * Выдача краткой информации о пользователе
 * 
 * @access public
 * @return string
 * @internal Магический метод, вызывается при попытке распечатать переменную с экземпляром класса. Например: echo $object;
 */	
	function __toString()
	{

return '<b>ФИО:</b> '.$this->userFullName.
	   '<br /> <b>ИИН:</b> '.$this->userIin.
	   '<br /> <b>E-mail:</b> '.$this->userEmail.
	   '<br /> <b>Тел.:</b> +7'.$this->userTel.
	   '<br /> <b>Должность:</b> '.($this->userAdmin==1?"Администратор":"Исполнитель").
	   '<br /> <b>Дата последнего входа:</b> '.date("H:i:s, d.m.Y",$this->userLastTime).' г. '.
	   '<br /> <b>Последний IP:</b> '.$this->userLastIP.
	   '<br /> <b>Последний UA:</b> '.$this->userLastUA;
	   
	}
/**
 * Проверка успешной авторизации
 * 
 * @access public
 * @return boolean
 * @internal Если авторизация прошла успешно, то возвращается true.
 */	
	public function isUser()
	{
		if($this->auth)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
/**
 * Проверка типа учетной записи
 * 
 * @access public
 * @return boolean
 * @internal Если пользователь имеет тип учетной записи admin, будет возвращено true
 */	
	public function isAdmin()
	{
		if($this->userAdmin==1)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
/**
 * Возвращает полное имя пользователя
 * 
 * @access public
 * @return string
 * @internal Возвращает значение свойства $userFullName
 */	
	public function getFullName()
	{
		if($this->auth)
		{
			return $this->userFullName;
		}
	}
/**
 * Возвращает идентификатор пользователя
 * 
 * @access public
 * @return integer
 * @internal Возвращает значение свойства $userId
 */	
	public function getUserId()
	{
		if($this->auth)
		{
			return $this->userId;
		}
	}
/**
 * Возвращает ИИН пользователя
 * 
 * @access public
 * @return integer
 * @internal Возвращает значение свойства $userIin
 */	
	public function getUserIin()
	{
		if($this->auth)
		{
			return $this->userIin;
		}
	}
/**
 * Возвращает Email пользователя
 * 
 * @access public
 * @return string
 * @internal Возвращает значение свойства $userEmail
 */	
	public function getUserEmail()
	{
		if($this->auth)
		{
			return $this->userEmail;
		}
	}
/**
 * Возвращает Tel.No пользователя
 * 
 * @access public
 * @return string
 * @internal Возвращает значение свойства $userTel
 */	
	public function getUserTel()
	{
		if($this->auth)
		{
			return $this->userTel;
		}
	}
/**
 * Возвращает timestamp последнего входа пользователя
 * 
 * @access public
 * @return long
 * @internal Возвращает значение свойства $userLastTime
 */		
	public function getUserLastTime()
	{
		if($this->auth)
		{
			return $this->userLastTime;
		}
	}
/**
 * Возвращает IP последнего входа пользователя
 * 
 * @access public
 * @return string
 * @internal Возвращает значение свойства $userLastIP
 */	
	public function getUserLastIP()
	{
		if($this->auth)
		{
			return $this->userLastIP;
		}
	}
/**
 * Возвращает User-Agent последнего входа пользователя
 * 
 * @access public
 * @return string
 * @internal Возвращает значение свойства $userLastUA
 */		
	public function getUserLastUA()
	{
		if($this->auth)
		{
			return $this->userLastUA;
		}
	}
/**
 * Уничтожает текущую сессию
 * 
 * @access public
 * @return void
 * @internal Уничтожает текущую сессию и выполняет редирект на страницу авторизации
 */		
	public function logOut()
	{
		session_unset();
		header("location: auth.php");
	}

/**
 * Destructor
 * @uses updateUserData()
 * @return void
 * @internal Деструктор класса
 */
	function __destruct()
	{
		if($this->auth)
		{
			$this->updateUserData();
		}
	}
/**
 * Обновляет данные о пользователе
 *
 * @access private
 * @return void
 * @internal Обновляет данные о пользователе: IP, UA, timestamp последнего входа
 */	
	private function updateUserData()
	{
		$ua=htmlspecialchars(trim($_SERVER['HTTP_USER_AGENT']));
		if($this->userLastIP!=$ua)
		{
			$this->connection->execute("UPDATE `people` SET `last_ua`='".$ua."' WHERE `id`='".$this->userId."'");
		}
		$ip=$_SERVER['REMOTE_ADDR'];
		if($this->userLastIP!=$ip)
		{
			$this->connection->execute("UPDATE `people` SET `last_ip`='".$ip."' WHERE `id`='".$this->userId."'");
		}
		$this->connection->execute("UPDATE `people` SET `last_time`='".time()."' WHERE `id`='".$this->userId."'");
	}
}
?>
