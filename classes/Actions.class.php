<?
/**
 * Класс выполнения различных вычислений приложения
 *
 * @author Binali Rustamov
 * @copyright	Copyright (c) 2014 Abitech
 * @license GPL License
 * @todo Расширение гибкости настройки, реализация интерфейса Iterator
 * @version 0.1 Alfa
 */
class Actions
{
/**
 * MySQL Object
 * 
 * @var object
 * @access private
 * @internal Свойство для хранения объекта класса MySQL
 */
	private $connection;
/**
 * MySQL Object
 * 
 * @var object
 * @access private
 * @internal Свойство для хранения объекта класса MySQL
 */
	private $user;
/**
 * Constructor
 * 
 * @param object MySQL $connection
 * @param object User  $user
 * @return void
 * @internal Конструктор класса
 */	
	function __construct($connection,$user)
	{
		$this->connection=$connection;
		$this->user=$user;
	}
/**
 * Валидация номера телефона
 *   
 * @access static
 * @param string $var
 * @return boolean
 * @internal Действие доступно, без создания экземпляра класса
 */	
	static function checkTel($var)
	{
			if(preg_match('/^[0-9]{5,15}$/u',$var))
			{
				return true;
			}
			else
			{
				return false;
			}
	}
/**
 * Валидация Email
 *   
 * @access static
 * @param string $var
 * @return boolean
 * @internal Действие доступно, без создания экземпляра класса
 */	
	static function checkEmail($var)
	{
	return preg_match('/^[_a-z0-9]+(\.[_a z0 9]+)*@[a-z0-9]+(\.[a-z0-9]+)*(\.[a-z]{2,3})$/i', $var);
	}
/**
 * Валидация ИИН
 *   
 * @access static
 * @param string $var
 * @return boolean
 * @internal Действие доступно, без создания экземпляра класса
 */	
	static function checkIin($var)
	{
		if(preg_match('/^[0-9]{5,12}$/u',$var))
			{
				return true;
			}
			else
			{
				return false;
			}
	}
/**
 * Валидация ФИО
 *   
 * @access static
 * @param string $var
 * @return boolean
 * @internal Действие доступно, без создания экземпляра класса
 */	
	static function checkFio($var)
	{
		if(preg_match('#^[А-яA-z ]+$#ui',$var))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
/**
 * Валидация пароля
 *   
 * @access static
 * @param string $var
 * @return boolean
 * @internal Действие доступно, без создания экземпляра класса
 */
	static function checkPassword($pass)
	{
		if(preg_match('/^[А-яA-z0-9]+$/i',$pass))
		{
			return true;
		}
		else
		{
			return false;
		}
	}
/**
 * Валидация комментария
 *   
 * @access static
 * @param string $var
 * @return boolean
 * @internal Действие доступно, без создания экземпляра класса
 */
	static function checkComment($var)
	{
		if(strlen($var)>2&&strlen($var)<=210)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
/**
 * Возвращает список заданий в виде массива
 *   
 * @access public
 * @param string  $type
 * @param integer $start
 * @param integer $limit
 * @uses $connection->query($SQL), $connection->getCount(), $connection->getRow(), $user->isAdmin(), $user->getUserId(), howLongTime()
 * @return array
 * @internal Возвращает массив с заданиями, определенной длинны ($limit) и с определенной позиции ($start).
 * @internal Параметр $type, указывает на тип возвращаемых заданий: new, lastcompleted, all.
 * @internal Выборка по $type имеет два запроса: администраторский и пользовательский.
 */
	public function getJobsArray($type='new',$start=0,$limit=10)
	{
		if($type=='lastcompleted')
		{
			if($this->user->isAdmin())
			{
				$query='SELECT * FROM `jobtime` WHERE `finish_time` IS NOT NULL AND `checked`=1';
			}
			else
			{
				$query="SELECT * FROM `jobtime` WHERE `finish_time` IS NOT NULL AND `executor_id`='".$this->user->getUserId()."'";
			}
		}
		elseif($type=='all')
		{
			if($this->user->isAdmin())
			{
				$query='SELECT * FROM `jobtime`';
			}
			else
			{
				$query="SELECT * FROM `jobtime` WHERE `executor_id`='".$this->user->getUserId()."'";
			}
			
		}
		else
		{
			if($this->user->isAdmin())
			{
			  $query='SELECT * FROM `jobtime` WHERE `finish_time` IS NOT NULL AND `checked`=0';
			}
			else
			{
			  $query="SELECT * FROM `jobtime` WHERE `finish_time` IS NULL AND `executor_id`='".$this->user->getUserId()."'";
			}
		}
		if($this->connection->query($query." LIMIT ".$start.",".$limit))
		{
		   $list=array();
		   if($this->user->isAdmin())
		   {
			 while($row=$this->connection->getRow())
			 {
				 $temp=array();
                 $temp['id']=$row['id'];
                 $temp['fio']=$this->getUserNameById($row['executor_id']);
                 $temp['start_time']=!empty($row['start_time'])?date('H:i, d.m.y',$row['start_time']):'';
                 $temp['finish_time']=!empty($row['finish_time'])?date('H:i, d.m.y',$row['finish_time']):'';		 
				 if(!empty($row['start_time']))
				 {
					$temp['jobtime']=(empty($row['finish_time'])?$this->howLongTime($row['start_time'],time()):$this->howLongTime($row['start_time'],$row['finish_time']));
				 }
				 else
				 {
					$temp['jobtime']=NULL;
				 }
				 if(!empty($row['author_comment']))
				 {
					$temp['author_comment']=$row['author_comment'];
				 }
				 else
				 {
				 	$temp['author_comment']=NULL; 
				 }
				 if(!empty($row['executor_comment']))
				 {
					$temp['executor_comment']=$row['executor_comment'];
				 }
				 else
				 {
				 	$temp['executor_comment']=NULL; 
				 }
				 $temp['checked']=$row['checked'];
				 if($row['checked']==1)
				 {
					$temp['status']='Выполнено, Проверенно';
			     }
			     elseif(!empty($row['finish_time']))
			     {
					$temp['status']='Выполнено, Проверка';
				 }
				 elseif(!empty($row['start_time']))
			     {
					$temp['status']='Выполняется';
				 }
				 else
				 {
					$temp['status']='Новое'; 
					$temp['editable']=true;
				 }
				$list[]=$temp; 
			 }  
		   }
		   else
		   {
			  while($row=$this->connection->getRow())
			 {
				 $temp=array();
                 $temp['id']=$row['id'];
				 $temp['start_time']=!empty($row['start_time'])?date('H:i, d.m.y',$row['start_time']):'';
                 $temp['finish_time']=!empty($row['finish_time'])?date('H:i, d.m.y',$row['finish_time']):'';	
				 if(!empty($row['start_time']))
				 {
					$temp['jobtime']=(empty($row['finish_time'])?$this->howLongTime($row['start_time'],time()):$this->howLongTime($row['start_time'],$row['finish_time']));
				 }
				 else
				 {
					$temp['jobtime']=NULL;
				 }
				 if(!empty($row['author_comment']))
				 {
					$temp['author_comment']=$row['author_comment'];
				 }
				 else
				 {
				 	$temp['author_comment']=NULL; 
				 }
				 if(!empty($row['executor_comment']))
				 {
					$temp['executor_comment']=$row['executor_comment'];
					
				 }
				 else
				 {
				 	$temp['executor_comment']=NULL; 
				 }
				 $temp['checked']=$row['checked'];
				 if($row['checked']==1)
				 {
					$temp['status']='Выполнено, Проверенно';
			     }
			     elseif(!empty($row['finish_time']))
			     {
					$temp['status']='Выполнено, Проверка';
				 }
				 elseif(!empty($row['start_time']))
			     {
					$temp['status']='Выполняется';
					$temp['started']=true;
				 }
				 else
				 {
					$temp['status']='Новое'; 
					$temp['isNew']=true;
				 }
				$list[]=$temp; 
			 }    
		   }
		   return $list;
		}
		else
		{
			return false;
		}
	}
/**
 * Возвращает число заданий
 *   
 * @access public
 * @param string  $type
 * @uses $connection->query($SQL), $connection->getResult(), $user->isAdmin(), $user->getUserId()
 * @return integer
 * @internal Возвращает число указанного типа заданий
 * @internal Параметр $type, указывает на тип заданий: new, lastcompleted, all.
 * @internal Выборка по $type имеет два запроса: администраторский и пользовательский.
 */
	public function getJobsCount($type='new')
	{
		if($type=='lastcompleted')
		{
			if($this->user->isAdmin())
			{
				$query='SELECT COUNT(*) FROM `jobtime` WHERE `finish_time` IS NOT NULL AND `checked`=1';
			}
			else
			{
				$query="SELECT COUNT(*) FROM `jobtime` WHERE `finish_time` IS NOT NULL AND `executor_id`='".$this->user->getUserId()."'";
			}
		}
		elseif($type=='all')
		{
			if($this->user->isAdmin())
			{
				$query='SELECT COUNT(*) FROM `jobtime`';
			}
			else
			{
				$query="SELECT COUNT(*) FROM `jobtime` WHERE `executor_id`='".$this->user->getUserId()."'";
			}
			
		}
		else
		{
			if($this->user->isAdmin())
			{
			  $query='SELECT COUNT(*) FROM `jobtime` WHERE `finish_time` IS NOT NULL AND `checked`=0';
			}
			else
			{
			  $query='SELECT COUNT(*) FROM `jobtime` WHERE `finish_time` IS NULL AND `checked`=0';
			}
		}
		$this->connection->query($query);
		$count=$this->connection->getResult();
		return $count;		
	}
/**
 * Возвращает разницу между двумя timestamp в форматированном виде
 *   
 * @access private
 * @param long  $time1
 * @param long  $time2
 * @return string
 * @internal Используется только в действии getJobsArray()
 */
	private function howLongTime($time1,$time2)
	{
		$time=$time2-$time1;
		if($time<60)
		{
			$value=ceil($time).'сек.';
		}
		elseif($time/60<60)
		{
			$value=ceil($time/60).'мин.';
		}
		elseif($time/3600<=24)
		{
			$value=ceil($time/3600).'часов';
		}
		else
		{
			$value=ceil($time/86400).'дней';
		}
	return $value;
	} 
/**
 * Возвращает имя пользователя по  ID
 *   
 * @access private
 * @param int  $id
 * @return string
 * @internal Используется только в действии getJobsArray()
 */	
	private function getUserNameById($id)
	{
		$row=mysql_fetch_array(mysql_query("SELECT `fio` FROM `people` WHERE `id`=".$id,$this->connection->getConnection()));
		return $row['fio'];
	}
/**
 * Возвращает список пользователей в виде массива
 *   
 * @access public
 * @param integer $start
 * @param integer $limit
 * @uses $connection->query($SQL), $connection->getCount(), $connection->getRow(), $user->isAdmin()
 * @return array
 * @internal Возвращает массив с пользователями, определенной длинны ($limit) и с определенной позиции ($start).
 * @internal Доступен только для администраторов ($user->isAdmin()=true)
 */
	public function getExecutorsArray($start=0,$limit=10)
	{
		if($this->user->isAdmin())
		{
			$this->connection->query('SELECT * FROM `people` ORDER BY `id` DESC LIMIT '.$start.','.$limit);
			if($this->connection->getCount()>0)
			{
				$list=array();
				while($row=$this->connection->getRow())
				{
					$temp=array();
					$temp['id']=$row['id'];
					$temp['fio']=$row['fio'];
					$temp['iin']=$row['iin'];
					$temp['email']=$row['email'];
					$temp['tel']=$row['tel'];
					$temp['status']=$row['admin'];
					if(!empty($row['last_time']))
					{
						$temp['last_time']=date("H:i, d.m.Y",$row['last_time']);
					}
					else
					{
						$temp['last_time']=NULL;
					}
					$list[]=$temp;
				}
				return $list;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
/**
 * Возвращает список пользователей в виде массива
 *   
 * @access public
 * @uses $connection->query($SQL), $connection->getResult(), $user->isAdmin()
 * @return integer
 * @internal Возвращает число всех сотрудников
 * @internal Доступен только для администраторов ($user->isAdmin()=true)
 */
	public function getExecutorsCount()
	{
		$this->connection->query('SELECT COUNT(*) FROM `people`');
		return $this->connection->getResult();
	}
/**
 * Проверяет существование пользователя
 *   
 * @access public
 * @param string $var
 * @param string $checkby
 * @uses $connection->query($SQL), $connection->getResult(), $user->isAdmin()
 * @return boolean
 * @internal Проверят существования пользователя по значению $var столбца $checkby
 * @internal Доступен только для администраторов ($user->isAdmin()=true)
 */
	public function isUser($var,$checkby='id')
	{
		if($this->user->isAdmin())
		{
			switch($checkby)
			{
				case 'id':
					$this->connection->query('SELECT COUNT(*) FROM `people` WHERE `id`='.(int) $var);
					if($this->connection->getResult()>0)
					{
						return true;
					}
					else
					{
						return false;
					}
				break;
				case 'email':
					$this->connection->query("SELECT COUNT(*) FROM `people` WHERE `email`='".mysql_escape_string($var)."'");
					if($this->connection->getResult()>0)
					{
						return true;
					}
					else
					{
						return false;
					}					
				break;
				case 'tel':
					$this->connection->query("SELECT COUNT(*) FROM `people` WHERE `tel`='".mysql_escape_string($var)."'");
					if($this->connection->getResult()>0)
					{
						return true;
					}
					else
					{
						return false;
					}
				break;
				case 'iin':
					$this->connection->query("SELECT COUNT(*) FROM `people` WHERE `iin`='".mysql_escape_string($var)."'");
					if($this->connection->getResult()>0)
					{
						return true;
					}
					else
					{
						return false;
					}
				break;
			}
		}
	}
/**
 * Удаляет пользователя по ID
 *   
 * @access public
 * @param integer $id
 * @uses $connection->execute($SQL), isUser(), $user->getUserId(),$user->isAdmin()
 * @return boolean
 * @internal Удаляет пользователя с ID=$id, при этом запрещает пользователю удалять самого себя.
 * @internal Доступен только для администраторов ($user->isAdmin=true)
 */
	public function deleteUser($id)
	{
		if($this->user->isAdmin()&&$this->isUser($id)&&(int) $id!=$this->user->getUserId())
		{
			if($this->connection->execute('DELETE FROM `people` WHERE `id`='.(int) $id))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
/**
 * Удаляет задания пользователя по USER ID
 *   
 * @access public
 * @param integer $user_id
 * @uses $connection->execute($SQL), isUser(), $user->getUserId(),$user->isAdmin()
 * @return boolean
 * @internal Удаляет задания пользователя с  USER ID=$id.
 * @internal Доступен только для администраторов ($user->isAdmin()=true)
 */	
	public function deleteUsersJob($user_id)
	{
		if($this->user->isAdmin()&&$this->isUser($user_id))
		{
			if($this->connection->execute('DELETE FROM `jobtime` WHERE `executor_id`='.(int) $user_id))
			{
				return true;
			}
			else
			{
				return false;
			}
		}
		else
		{
			return false;
		}
	}
/**
 * Создает пользователя
 *   
 * @access public
 * @param string $fio
 * @param string $iin
 * @param string $email
 * @param string $tel
 * @param integer $admin
 * @param string $password
 * @uses $connection->execute($SQL), $user->isAdmin()
 * @return boolean
 * @internal Создает нового пользователя в базе данных
 * @internal Доступен только для администраторов ($user->isAdmin()=true)
 */	
	public function createUser($fio,$iin,$email,$tel,$admin,$password)
	{	if($this->user->isAdmin())
		{
			return $this->connection->execute("INSERT INTO `people` (`fio`,`iin`,`email`,`tel`,`admin`,`password`) VALUES ('".mysql_escape_string($fio)."','".mysql_escape_string($iin)."','".mysql_escape_string($email)."','".mysql_escape_string($tel)."','".(int) $admin."','".md5($password)."')");
		}
		else
		{
			return false;
		}
	}
/**
 * Обновляет данные пользователя
 *   
 * @access public
 * @param integer $id
 * @param string $value
 * @param string $column
 * @uses $connection->execute($SQL), $user->isAdmin()
 * @return boolean
 * @internal Обновляет значение $value столбца $column данных пользователя
 * @internal Доступен только для администраторов ($user->isAdmin()=true)
 */	
	public function updateUser($id,$value,$column='fio')
	{
	if($this->user->isAdmin())
	{
		return $this->connection->execute("UPDATE `people` SET `".$column."`='".mysql_escape_string($value)."' WHERE `id`='".(int)$id."'");
	}
	}
/**
 * Возвращает данные о пользователе в виде массива
 *   
 * @access public
 * @param integer $id
 * @uses $connection->query($SQL), $connection->getRow(),$user->isAdmin()
 * @return array
 * @internal Найдя пользователя по $id, возвращает массив с данными
 * @internal Доступен только для администраторов ($user->isAdmin()=true)
 */	
	public function getUserAsArray($id)
	{
		if($this->user->isAdmin()&&$this->isUser($id))
		{
			$this->connection->query("SELECT `id`,`fio`,`iin`,`tel`,`admin` FROM `people` WHERE `id`='".(int)$id."'");
			return $this->connection->getRow();
		}
	}
/**
 * Удаляет задание по ID
 *   
 * @access public
 * @param integer $id
 * @uses $connection->execute($SQL), $user->isAdmin()
 * @return boolean
 * @internal Удаляет задание с ID=$id
 * @internal Доступен только для администраторов ($user->isAdmin()=true)
 */
	public function deleteJob($id_job)
	{
	if($this->user->isAdmin())
	{
		return $this->connection->execute("DELETE FROM `jobtime` WHERE `id`='".(int) $id_job."'");
	}
	}
/**
 * Отмечает задание как выполняемое
 *   
 * @access public
 * @param integer $id_job
 * @uses $connection->execute($SQL), $user->isAdmin(), $user->getUserId()
 * @return boolean
 * @internal Отмечает задание с ID=$id как выполняемое
 * @internal Доступен только для пользователей ($user->isAdmin()=false)
 */
	public function startJob($id_job)
	{
		if(!$this->user->isAdmin())
		{
			$this->connection->query("SELECT COUNT(*) FROM `jobtime` WHERE `id`='".(int) $id_job."' AND `executor_id`='".$this->user->getUserId()."' AND `start_time` IS NULL");
			if($this->connection->getResult()>0)
			{
				$this->connection->execute("UPDATE `jobtime` SET `start_time`='".time()."' WHERE `id`='".(int) $id_job."'");
			}
		}
	}
/**
 * Отмечает задание как выполненное
 *   
 * @access public
 * @param integer $id_job
 * @uses $connection->execute($SQL), $user->isAdmin(), $user->getUserId()
 * @return boolean
 * @internal Отмечает задание с ID=$id как выполненное
 * @internal Доступен только для пользователей ($user->isAdmin()=false)
 */
	public function finishJob($id_job,$executor_comment)
	{
		if(!$this->user->isAdmin())
		{
			$this->connection->query("SELECT COUNT(*) FROM `jobtime` WHERE `id`='".(int) $id_job."' AND `executor_id`='".$this->user->getUserId()."' AND `start_time` IS NOT NULL AND `finish_time` IS NULL");
			if($this->connection->getResult()>0)
			{
				$this->connection->execute("UPDATE `jobtime` SET `finish_time`='".time()."',`executor_comment`='".mysql_escape_string($executor_comment)."' WHERE `id`='".(int) $id_job."'");
			}
		}
	}
/**
 * Отмечает задание как проверенное
 *   
 * @access public
 * @param integer $id_job
 * @uses $connection->execute($SQL), $user->isAdmin()
 * @return boolean
 * @internal Отмечает задание с ID=$id как выполненное
 * @internal Доступен только для администраторов ($user->isAdmin()=true)
 */
	public function checkJob($id_job)
	{
		if($this->user->isAdmin())
		{
			$this->connection->query("SELECT COUNT(*) FROM `jobtime` WHERE `id`='".(int) $id_job."' AND `finish_time` IS NOT NULL AND `checked`='0'");
			if($this->connection->getResult()>0)
			{
				$this->connection->execute("UPDATE `jobtime` SET `checked`='1' WHERE `id`='".(int) $id_job."'");
			}
		}
	}
/**
 * Создает задание
 *   
 * @access public
 * @param integer $executor_id
 * @param string  $author_comment
 * @uses $connection->execute($SQL), $user->isAdmin()
 * @return boolean
 * @internal Создает задание для пользователя с USER ID=$executor_id, c комментарием $author_comment
 * @internal Доступен только для администраторов ($user->isAdmin()=true)
 */
	public function createJob($executor_id,$author_comment)
	{
		if($this->user->isAdmin())
		{
		return $this->connection->execute("INSERT INTO `jobtime` (`executor_id`,`author_comment`) VALUES ('".(int) $executor_id."','".mysql_escape_string($author_comment)."')");
		}
	}
/**
 * Обновляет задание
 *   
 * @access public
 * @param integer $id
 * @param string $value
 * @param string $column
 * @uses $connection->execute($SQL), $user->isAdmin()
 * @return boolean
 * @internal Обновляет значение $value столбца $column данных задания
 * @internal Доступен только для администраторов ($user->isAdmin()=true)
 */
	public function updateJob($id,$value,$column='executor_id')
	{
		if($this->user->isAdmin())
		{
			return $this->connection->execute("UPDATE `jobtime` SET `".$column."`='".mysql_escape_string($value)."' WHERE `id`='".(int) $id."'");
		}
	}
/**
 * Возвращает массив с полными именами всех сотрудников
 *   
 * @access public
 * @uses $connection->query($SQL), $connection->getCount(), $connection->getRow(),$user->isAdmin()
 * @return array
 * @internal Возвращает массив вида $arr[id_сотрудника]=полное_имя_сотрудника
 * @internal Доступен только для администраторов ($user->isAdmin()=true)
 */
	public function getUserNamesAsArray()
	{
		if($this->user->isAdmin())
		{
		   $this->connection->query("SELECT `id`,`fio` FROM `people` WHERE `id`!='".$this->user->getUserId()."'");
		   if($this->connection->getCount()>0)
		   {
				$list=array();
				while($row=$this->connection->getRow())
				{
					$list[$row['id']]=$row['fio'];
				}
				return $list;
		   }
		}
	}
/**
 * Проверяет существование задания
 *   
 * @access public
 * @param integer $id_job
 * @uses $connection->query($SQL), $connection->getResult(),$user->isUser()
 * @return boolean
 * @internal Проверяет существование задания по ID.
 * @internal Доступен только для авторизованных ($user->isUser()=true)
 */
	public function isJob($id_job)
	{
		if($this->user->isUser())
		{
		$this->connection->query("SELECT COUNT(*) FROM `jobtime` WHERE `id`='".(int)$id_job."'");
		return $this->connection->getResult();
		}
		else
		{
		return false;
		}
	}
/**
 * Возвращает данные о задании в виде массива
 *   
 * @access public
 * @param integer $id_job
 * @uses $connection->query($SQL), $connection->getRow(),$user->isAdmin()
 * @return array
 * @internal Найдя задание по $id, возвращает массив с данными
 * @internal Доступен только для администраторов ($user->isAdmin()=true)
 */	
	public function getJobById($id_job)
	{
		if($this->user->isAdmin())
		{
		$this->connection->query("SELECT `executor_id`,`author_comment`,`id` FROM `jobtime` WHERE `id`='".(int)$id_job."'");
		return $this->connection->getRow();
		}
	}
/**
 * Destructor
 * @return void
 * @internal Деструктор класса
 */
	function __destruct()
	{
		if(isset($this->connection)) unset($this->connection);
		if(isset($this->user)) unset($this->user);
	}
	
}
?>
