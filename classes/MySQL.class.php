<?
/**
 * Класс работы с СУБД MySQL
 *
 * @author Binali Rustamov
 * @copyright	Copyright (c) 2014 Abitech
 * @license GPL License
 * @todo Расширение гибкости настройки, реализация интерфейса Iterator
 * @version 0.1 Alfa
 */
class MySQL 
	{
/**
 * Connection ID
 * 
 * @var integer
 * @access private
 * @internal Свойство для хранения идентификатора соединения с MySQL
 */
		private $connection;
/**
 * Query Resource
 * 
 * @var resource 
 * @access private
 * @internal Свойство для хранения ресурса выполненного запроса MySQL
 */
		private $queryRes;
/**
 * Array Of Errors
 * 
 * @var array 
 * @access private
 * @internal Свойство для хранения ресурса выполненного запроса MySQL
 */
		private $currentError;
/**
 * Current SQL Command
 * 
 * @var string 
 * @access private
 * @internal Свойство для хранения текста текущего запроса MySQL
 */
		private $currentSQLCommand;
/**
 * Constructor
 * 
 * @param string $connection
 * @param string $user
 * @param string $password
 * @param string $db
 * @uses execute($SQL)
 * @return void
 * @internal Конструктор класса
 */
		function __construct($host,$user,$password,$db)
		{
				$this->connection=mysql_connect($host,$user,$password,$db);
				if($this->connection==false)
				{
					$this->current_error[]="Произошла ошибка при подключении к серверу MySQL, проверьте данные.";
					return false;
				}
				elseif(!mysql_select_db($db,$this->connection))
				{
					$this->currentError[]="Произошла ошибка при выборе БД.";
				}
				else
				{
					$this->execute('SET CHARSET utf8');
					$this->execute('SET NAMES utf8');
				}
		}
/**
 * Выполнения запроса для обновления
 * 
 * @access public
 * @param string $SQL
 * @return boolean
 * @internal Действие, для выполнение запросов типа UPDATE, INSERT, DELETE, и т.п.
 */	
		public function execute($SQL)
		{
				if(isset($this->queryRes))
				{
					unset($this->queryRes);
			    }
				if(mysql_query($SQL,$this->connection))
				{
					return true;		
				}
				else
				{
					unset($this->currentError);
					$this->currentError[]='Произошла ошибка при выполнении команды, повторте еще раз.';
					return false;
				}
		}
/**
 * Выполнение запроса выборки
 *   
 * @access public
 * @param string $SQL
 * @return boolean
 * @internal Действие, для выполнение запросов типа SELECT, и т.п.
 */		
		public function query($SQL)
		{
				if(isset($this->queryRes))
				{
					unset($this->queryRes);
			    }
				$this->queryRes=mysql_query($SQL,$this->connection);
				if($this->queryRes)
				{
					$this->currentSQLCommand=$SQL;
					return true;		
				}
				else
				{
					unset($this->currentError);
					$this->currentError[]='Произошла ошибка при выполнении команды, повторте еще раз.';
					return false;
				}
		}
/**
 * Возвращает одну запись из результата выборочного запроса и сдвигает позицию курсора на следующую запись  
 *   
 * @access public
 * @return mixed
 * @internal Использовать только совмество с действием query()
 */	
		public function getRow()
			{
				if(isset($this->queryRes))
				{
					return mysql_fetch_array($this->queryRes);
				}
		    }
/**
 * Возвращает количество записей результата выборки
 *   
 * @access public
 * @return integer|boolean
 * @internal Использовать только совмество с действием query()
 */			
		public function getCount()
			{
				if(isset($this->queryRes))
				{
					
					return mysql_num_rows($this->queryRes);
				}
			}

		public function getResult()
			{	if(isset($this->queryRes))
				{
/**
 * Возвращает значение 0-го столбца результата выборки
 *   
 * @access public
 * @return mixed
 * @internal Использовать только совмество с действием query()
 */					
					return mysql_result($this->queryRes,0);
				}
			}
/**
 * Возвращает идентификатор соединения с СУБД MySQL
 *   
 * @access public
 * @return integer
 * @internal Возвращает значание свойства $connection
 */	
		public function getConnection()
			{
				if(isset($this->connection))
				{
					return $this->connection;
				}
			}
/**
 * Возвращает массив с ошибками
 *   
 * @access public
 * @return array
 * @internal Возвращает значание свойства $currentError
 */	
		public function getErrors()
			{
				if(isset($this->currentError))
				{
					return $this->currentError;
				}
			}
/**
 * Destructor
 *   
 * @return void
 * @internal Деструктор класса, закрывает соединение
 */			
		function __destruct()
			{
				if(isset ($this->queryRes))
				{
					unset($this->queryRes);
				}
				if(isset($this->conncetion))
				{
					mysql_close($this->connection);
					unset($this->connection);
				}
			}
	}
?>
